from bottle import route, run, template
from os import environ


@route('/hello/<name>')
def hello(name):
    return template('<b>Hello {{name}}</b>!', name=name)


@route('/multiply/<a>/<b>')
def route_multiply(a, b):
    result = multiply(a, b)
    return template('<b>Result of {{a}} * {{b}} is {{result}}</b>!', a=a, b=b, result=result)


def multiply(a, b):
    return int(a) * int(b)


def addition(a, b):
    return a + b


@route('/')
def index():
    return '<b>Bonjour d\'Heroku !</b>'


@route('/users')
def users():
    users_list = ['Alexandre', 'Brenda', 'Pierre', 'etc']
    output = template('template', users=users_list)
    return output


if __name__ == '__main__':
    run(host='0.0.0.0', port=environ.get('PORT', 8080))
